#+TITLE: Dots Config Legacy

* Guix Config
   #+begin_src scheme

   (locale "pt_BR.utf8")

   (locale-definitions
    (list (locale-definition (source "en_US") (name "en_US.utf8"))
	  (locale-definition (source "pt_BR") (name "pt_BR.utf8"))))

   ("/usr/bin/sh"
    ,(file-append (canonical-package coreutils)
		  "/bin/sh"))
   ("/usr/bin/bash"
    ,(file-append (canonical-package coreutils)
		  "/bin/bash"))

   ("/bin/pwd"
    ,(file-append (canonical-package coreutils)
		  "/bin/pwd"))


   ("/bin/startx" ,(xorg-start-command))

   #+end_src

* stumpwm
#+begin_src lisp
;; (defun executables ()
;;   (loop with path = (uiop:getenv "PATH")
;;         for p in (uiop:split-string path :separator ":")
;;         for dir = (probe-file p)
;;         when (uiop:directory-exists-p dir)
;;           append (uiop:directory-files dir)))

;; (defun find-executable (name)
;;   (find name (executables)
;;         :test #'equalp
;;         :key #'pathname-name))

;; (defun eas/anyexec (lst)
;;   "Return first executable that exist in lst"
;;   (dolist (current lst)
;;     (when (eas/commandv current)
;;       current)))


;; (defun eas/run-app (cmd prop &optional args) ;; FIX: fix
;;   "Run an instance of `cmd' with property `prop' (and any optional arguments `args')"
;;   (if (null args)
;;       (run-or-raise cmd prop)
;;       (run-or-raise (cat cmd " " args) prop)))

;; ;; (defcommand run-editor () ()
;;   "Run an instance of `*editor*' with property`:instance'."
;;   (eas/run-app *editor* (list :instance *editor*)))

;; (defcommand run-ide () ()
;;   "Run an instance of `*ide*' with property`:instance'."
;;   (eas/run-app *ide* (list :instance *ide*)))

;; (defcommand run-browser () ()
;;   "Run an instance of `*browser*' with property`:instance'."
;;   (eas/run-app *browser* (list :instance *browser*)))

;; (defcommand run-terminal () ()
;;   "Run an instance of `*terminal*' with property`:instance'."
;;   (eas/run-app *terminal* (list :instance *terminal*)))

;; (defcommand run-locker () ()
;;   "Run an instance of `*locker*' with property`:instance'."
;;   (eas/run-app *locker* (list :instance *locker*)))

;; (define-key *top-map* (kbd "s-RET") "run-terminal")
;; (define-key *top-map* (kbd "s-l") "run-locker")
;; (define-key *top-map* (kbd "s-b") "run-browser")
;; (define-key *top-map* (kbd "s-e") "run-editor")

;; -----------------
;; EXTERNAL SOFTWARE
;; -----------------

;; GLOBAL MACROS
;; (defmacro search-on-web (name url-prefix)
;;   `(defcommand ,name (search)
;;      ((:rest ,(concatenate 'string (symbol-name name) ": ")))
;;      (run-shell-command (format nil "~A ~A"
;; 				*browser*
;; 				(concat ,url-prefix (substitute #\+ #\Space search))))))

;; (search-on-web google "http://www.google.com/search?q=")
;; (search-on-web wikipedia "http://en.wikipedia.org/wiki/Special:Search?fulltext=Search&search=")
;; (search-on-web youtube "http://youtube.com/results?search_query=")

;; (defun runner (program &optional args)
;;   ".NET like Runnner."
;;   (uiop:run-program (concatenate 'string program " " args)))


;; (defcommand tocador () ()
;;   (let ((link (trivial-clipboard:text))
;; 	(player "mpv")
;; 	(args "--no-config --no-audio-display"))
;;     (runner "mpv" (concatenate 'string args
;; 			       " "
;; 			       link))))
;; (define-key *top-map* (kbd "s-P") "tocador")

;; ;; WALLPAPER
;; (defun waller()
;;   (let ((setter "feh")
;; 	(setter-args "--randomize --bg-fill")
;; 	(wallpapers (concatenate 'string *pictures* "/papelparede")))
;;     (run-shell-command (concatenate 'string  setter " " setter-args " " wallpapers))))

;; (when (eas/commandv "feh")
;;   (waller))

;; -----------------
;; CUSTOM COMMANDS
;; -----------------

;; (defcommand safe-quit () ()
;;   "Checks if any windows are open before quitting."
;;   (let ((win-count 0)) ;; count the windows in each group
;;     (dolist (group (screen-groups (current-screen)))
;;       (setq win-count (+ (length (group-windows group)) win-count)))
;;     (if (= win-count 0) ;; display the number of open windows or quit
;; 	(run-commands "quit")
;; 	(message (format nil "You have ~d ~a open" win-count
;; 			 (if (= win-count 1) "window" "windows"))))))

#+end_src
* Nixos
** Config
    #+begin_src nix
    #efiInstallAsRemovable = true; # in case canTouchEfiVariables doesn't work for your system

    #boot.loader.systemd-boot.enable = true;

	  # services.xserver.xkbOptions = "eurosign:e";

	  # Some programs need SUID wrappers, can be configured further or are
	  # started in user sessions.
	  # programs.mtr.enable = true;
	  # programs.gnupg.agent = {
	  #   enable = true;
	  #   enableSSHSupport = true;
	  #   pinentryFlavor = "gnome3";
	  # };

	  # * Firewall
	  # networking.firewall.allowedTCPPorts = [ ... ];
	  # networking.firewall.allowedUDPPorts = [ ... ];
	  # Or disable the firewall altogether.
	  # networking.firewall.enable = false;

	  # Enable CUPS to print documents.
	  # services.printing.enable = true;

    # Configure network proxy if necessary
    # networking.proxy.default = "http://user:password@proxy:port/";
    # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    # Select internationalisation properties.
    # i18n.defaultLocale = "en_US.UTF-8";
    # console = {
    #   font = "Lat2-Terminus16";
    #   keyMap = "us";
    # };

    #+end_src
* Nyxt
   #+begin_src conf ~/.config/nyxt/init.lisp :mkdirp yes
   (in-package :next-user)

   ;; Search Engines
   (defvar a/search-engines
     '(("bi" . "https://bing.com/?q=~a")
       ("dg" . "https://duckduckgo.com/?q=~a")
       ("g" . "https://www.google.com/search?ion=1&q=~a")
       ("gh" . "https://github.com/search?ref=simplesearch&q=~a")
       ("q" .  "http://quickdocs.org/search?q=~a")
       ("s" .  "http://stackoverflow.com/search?q=~a")
       ("wp" . "http://www.wikipedia.org/search-redirect.php?language=en&go=Go&search=~a")
       ("yt" . "https://www.youtube.com/results?search_query=~a")))

   (defclass my-browser (gtk-browser)
     ((search-engines :initform
		      (append
		       a/search-engines
		       (get-default 'browser 'search-engines)))))

   (setf *browser-class* 'my-browser)
   #+end_src
** Config
    #+begin_src lisp
    (defvar *my-keymap* (make-keymap)  "My keymap.")

    (define-command play-page-video (&optional (buffer (current-buffer)))
      "Play video in the currently open buffer."
      (uiop:run-program (list "mpv" (url buffer))))
    (define-key :keymap *my-keymap*  "C-M-c v" #'play-page-video)

    (define-command play-video-in-current-page (&optional (buffer (current-buffer)))
      "Play video in the currently open buffer."
      (uiop:run-program (list "mpv" (url buffer))))

    (defvar *my-keymap* (make-keymap)
      "My keymap.")

    (define-mode my-mode ()
      "Dummy mode for the custom key bindings in `*my-keymap*'."
      ((keymap-schemes :initform (list :emacs *my-keymap*
				       :vi-normal *my-keymap*))))

    Debugging
    (setf *swank-port* 4006)


    Use development platform port.
    (setf +platform-port-command+
	  "~/.local/bin/next-gtk-webkit")

    open-file
    (defun my-open-videos (filename)
      "Open videos with mpv."
      (handler-case (let ((extension (pathname-type filename)))
		      (match extension
			     ((or "webm" "mkv" "mp4")
			      (uiop:launch-program (list "mpv" filename)))
			     (_
			      (next/file-manager-mode:open-file-function filename))))
	(error (c) (log:error "Error opening ~a: ~a" filename c))))

    (setf next/file-manager-mode:*open-file-function* #'my-open-videos)


    (define-key :keymap *my-keymap* "C-M-c v" #'play-video-in-current-page)


    -- a-FUNCTIONS (m-x)
    (defun a-play-video ()
      "Play current page's video"
      (with-result (url (buffer-get-url))
	(uiop:launch-program (list "mpv" url))))

    (define-command a-get-video ()
      "Download current page's video"
      (with-result (url (buffer-get-url))
	(uiop:launch-program (list "youtube-dl" url "&"))))

    (define-command a-bookmark-url ()
      "Allow the user to bookmark a URL via minibuffer input."
      (with-result (url (read-from-minibuffer (minibuffer *interface*)))
	(%bookmark-url url)))

    ;; Zoom
    ;; (setf *zoom-ratio-default* 1.6)

    HOME PAGE
    (setf (get-default 'remote-interface 'start-page-url) "https://cnn.com")

    ;; Minibuffer
    (setf (get-default 'minibuffer 'minibuffer-style)
	  (cl-css:inline-css
	   '((body :border-top "14px solid red"))))

    #+end_src
* gtk-3.0
   #+begin_src conf
   [Settings]
   gtk-fallback-icon-theme=Numix
   gtk-icon-theme-name=Canta
   gtk-key-theme-name=Emacs
   gtk-theme-name=Canta
   #+end_src
* pycodestyle
   #+begin_src conf
   [pycodestyle]
   max-line-length = 90
   #+end_src
* flake
   #+begin_src conf
   [flake8]
   max-line-length = 88
   exclude = tests/*
   max-complexity = 10
   #+end_src
* tmux
   #+begin_src conf
   set-option -g default-shell "/usr/bin/bash"

   24-bit color
   set -ga terminal-overrides ",xterm-termite:Tc"

   24 Colors
   set -g default-terminal "screen-256color-italic"
   set-option -ga terminal-overrides ",xterm-256color*:Tc:smso"

   mouse
   bind-key m set-option -g mouse on \; display 'Mouse: ON'
   bind-key M set-option -g mouse off \; display 'Mouse: OFF'

   #+end_src
* fontconfig
   #+begin_src conf
   <?xml version="1.0" encoding="UTF-8"?>
   <!DOCTYPE fontconfig SYSTEM "fonts.dtd">
   <fontconfig>
     <alias>
       <family>serif</family>
       <prefer>
	 <family>Noto Color Emoji</family>
       </prefer>
     </alias>
     <alias>
       <family>sans-serif</family>
       <prefer>
	 <family>Noto Color Emoji</family>
       </prefer>
     </alias>
     <alias>
       <family>monospace</family>
       <prefer>
	 <family>Noto Color Emoji</family>
       </prefer>
     </alias>
   </fontconfig>
   #+end_src
