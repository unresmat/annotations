#+TITLE: Vue.js

* Progress
** Books
| Books                   | Cap                         | @                                |
|-------------------------+-----------------------------+----------------------------------|
| Front-end dev w/ Vue.js | 2                           | Deep Watching Concepts           |
| Fullstack Vue           | II - Single-file components | Breaking the app into components |

** SLOC
| SLOC | folder | file | @ |
|------+--------+------+---|
| vue  |        |      |   |
* Base
** Directives
*** v-bind
#+begin_src html
<article v-for="submission in submissions" v-bind:key="submission.id"
  class="media">
  <!-- Rest of the submission template -->
</article>
#+end_src

#+begin_src html
<!-- // the full syntax -->
<img v-bind:src="submission.submissionImage" />
<!-- // the shorthand syntax -->
<img :src="submission.submissionImage" />
#+end_src

*** v-for
#+begin_src html
<article v-for="submission in submissions" class="media">
  <!-- Rest of the submission template -->
</article>
#+end_src
*** v-on
create event listeners within the DOM.

#+begin_src html
<div class="media-right">
  <span class="icon is-small" v-on:click="upvote(submission.id)">
    <i class="fa fa-chevron-up"></i>
    <strong class="has-text-info">{{ submission.votes }}</strong>
  </span>
</div>
#+end_src

#+begin_src html
<!-- // the full syntax -->
<span v-on:click="upvote(submission.id)"></span>
<!-- // the shorthand syntax -->
<span @click="upvote(submission.id)"></span>
#+end_src
*** v-if
** Data Properties (Props)

#+begin_src html
<template>
    <div>{{color}}</div>
</template>
<script>
    export default {
        data() {
          return {
            color: 'red'
          }
        }
    }
</script>
#+end_src
** Components
Vue components are Vue instances.

*** props
*** hooks
*beforeCreate*

*created*

*beforeMount*

*mounted*

*beforeUpdate*


*updated*

*** Single-File Components
** Data Binding
** Directives
*v-text*

*v-once*

*v-html*

#+begin_src html
<template>
  <div>
    <h1 v-once v-text="text">Loading...</h1>
    <h2 v-html="html" />
  </div>
</template>
<script>
export default {
  data() {
    return {
      // v-text
      text: 'Directive text',
      // v-html
      html: 'Stylise</br>HTML in<br/><b>your data</b>',
    }
  },
}
</script>
#+end_src

*v-bind*

*v-if*

*v-else*

*v-else-if*

#+begin_src html
<template>
  <div>
    <h1 v-if="false" v-once v-text="text">Loading...</h1>
    <h2 v-else-if="false" v-html="html" />
    <a
      v-else
      :href="link.url"
      :target="link.target"
      :tabindex="link.tabindex"
      v-text="link.title"
    />
  </div>
</template>
#+end_src

*v-show*

#+begin_src html
<template>
  <div>
    <h1 v-show="true" v-once v-text="text">Loading...</h1>
    <h2 v-show="false" v-html="html" />
    <a
      :href="link.url"
      :target="link.target"
      :tabindex="link.tabindex"
      v-text="link.title"
    />
  </div>
</template>
#+end_src

*v-for*

#+begin_src html
<ul>
    <li v-for="n in 5" :key="n">{{ n }}</li>
</ul>
#+end_src

#+begin_src html
<template>
  <div>
    <h1>Looping through arrays</h1>
    <ul>
      <li v-for="(item, n) in interests" :key="n">
        {{ item }}
      </li>
    </ul>
  </div>
</template>
<script>
export default {
  data() {
    return {
      interests: ['TV', 'Games', 'Sports'],
    }
  },
}
</script>
#+end_src

*v-model*

#+begin_src html
<template>
    <input v-model="name" />
</template>
<script>
      export default {
        data() {
          return {
            name: ''
          }
        }
      }
</script>
#+end_src
** Computed Properties
** Computed Setters
** Watchers

* Commands
** add
** serve
* Tips
** container

#+begin_src shell
docker run --rm -d -v $(pwd):/app/ -v /app/node_modules -w /app -p 8081:8080 debian:unstable bash -c 'apt-get update && apt install -y nodejs npm && npm i -g yarn && yarn && yarn serve'
#+end_src
