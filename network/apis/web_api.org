#+TITLE: Web APIs

* Summary
  :PROPERTIES:
  :TOC:      :include all :depth 2 :ignore this
  :END:
:CONTENTS:
- [[#progresso][Progresso]]
- [[#goals][Goals]]
- [[#soap][SOAP]]
  - [[#benefits][benefits]]
  - [[#mm][mm]]
  - [[#soap-message][Soap Message]]
- [[#rest][REST]]
- [[#graphql][GraphQL]]
- [[#grpc][gRPC]]
:END:
* Progresso
| books                  | chapter                             | at |
|------------------------+-------------------------------------+----|
| The Design of Web APIs | 3 Designing a programming interface |    |

* Goals
- Whos: Where you list the API’s users (or profles)
- Whats: Where you list what these users can do
- Hows: Where you decompose each of the what’s into steps
- Inputs (source): Where you list what is needed for each step and where it comes from (to spot missing whos, whats, or hows)
- Output (usage): Where you list what is returned by each step and how it is used (to spot missing whos, whats, or hows)
- Goals: Where you explicitly and concisely reformulate each how + inputs + outputs

* SOAP
 - Web Service Description Language
 - uses a standard XML schema (XSL) to encode XML
 - is a protocol for exchanging information encoded in Extensible Markup Language (XML) between a client and a procedure or service that resides on the Internet.
 - used by a variety of transport protocols in addition to HTTP, for example, FTP and SMTP
 - HTTP for synchronous data exchange and SMTP or FTP for asynchronous interactions

** benefits
- implemented using a variety of protocols
** mm
- considered a complex message format with a lot of ins and outs to the specification.
- verbose nature of XML which is the format upon which SOAP is based, coupled with the reliance on external namespaces to extend the basic message format makes the protocol bulky and at times difficult to manage.
- can get quite large
- legacy protocol.
-

** Soap Message
   - hierarchical structure in which the root element is the <soap:Envelope>
   - root element can have three child elements
   - <soap:Header>, <soap:Body> and <soap:Fault>
   - <soap:Body> is required
   - optional element, <soap:Header> is used, it must be the first child element within the parent element,
   - <soap:Envelope> and when the optional element <soap:Fault> is used, it must be a child of the element, <soap:Body>.
    -<soap:Header> provides header information relevant to the message.
   - <soap:Body> describes the payload of the message
   - element <soap:Fault> contains error information that’s occurred during the transmission and consumption of the message.
   - SOAPAction is an HTTP header attribute that tells the host that the incoming request is a SOAP message.

#+begin_src xml
 POST /BobsTickers HTTP/1.1
 Host: www.example.org
 Content-Type: application/soap+xml; charset=utf-8
 Content-Length: 275
 SOAPAction: "http://cooltickers.org/soap"
 
 <?xml version="1.0"?>
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:m="http://www.exampletickers.org">
  <soap:Header>
  </soap:Header>
  <soap:Body>
    <m:GetStockPriceRequest>
      <m:StockName>IBM</m:StockName>
    </m:GetStockPriceRequest>
  </soap:Body>
</soap:Envelope>
#+end_src
* REST
- Representational State Transfer
- use the standard HTTP methods, GET, POST, PUT and DELETE, to query and mutate resources represented by URIs on the Internet.
- resource are big dataset that describes a collection of entities of the type
- agnostic in terms of the format used to structure the response data from a resource.
- HTTP/1.1 is the protocol used
- stateless request-response mechanism
- supports the concept of Hypermedia as the Engine of Application State (HATEOAS).
- Productivity under REST is almost immediate.
- clunky and immutable in terms of the data structure of a response.
- can be slow.
- limited rule set to follow.

#+begin_src json
{
  "car": {
    "vin": "KNDJT2A23A7703818",
    "make": "kia",
    "model": "soul",
    "year": 2010,
    "links": {
      "service": "/cars/KNDJT2A23A7703818/service",
      "sell": "/cars/KNDJT2A23A7703818/sell",
      "clean": "/cars/KNDJT2A23A7703818/sell"
    }
  }
}
#+end_src
* GraphQL
- is a specification
- represent data in a graph
- graph database is a collection of nodes and edges.
- types, queries, and mutations are defined in the schema
- flexible in defining the structure of the data that's returned when making a query against the API.
- Subscriptions open the door to asynchronous messaging.
- Query and mutation data exchange are synchronous due to the request-response pattern inherent in the HTTP/1.1 protocol.
- Synchronous and asynchronous activities are distinct.
- 2 servers: synchronous HTTP server and also an asynchronous subscription server.
-  While the specification allows for customization, the basic framework cannot be avoided.

* gRPC
 - it's a specification
 - uses the Protocol Buffers binary format.
 - requires that both the client and server in a gRPC data exchange have access to the same schema definition.
 - Protocol Buffers definition in a .proto file.
 - binary format as the means of data exchange is that it increases performance
 - supports bidirectional, asynchronous data exchange.
 - based on the HTTP/2 protocol.
 - HTTP/2 allow gRPC to be used in situations where REST or GraphQL can't even be considered.
