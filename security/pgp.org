#+TITLE: PGP - Pretty Good Privacy

* gpg
gpg is the OpenPGP part of the GNU Privacy Guard (GnuPG). It is a tool to
provide digital encryption and signing services us‐ ing the OpenPGP
standard. gpg features complete key management and all the bells and
whistles you would expect from a full OpenPGP implementation.

There are two main versions of GnuPG: GnuPG 1.x and GnuPG 2.x. GnuPG 2.x
supports modern encryption algorithms and thus should be preferred over
GnuPG 1.x. You only need to use GnuPG 1.x if your platform doesn't
support GnuPG 2.x, or you need support for some features that GnuPG 2.x
has deprecated, e.g., decrypting data created with PGP-2 keys.

If you are looking for version 1 of GnuPG, you may find that version
installed under the name gpg1.
